# remd-cli
`remd-cli` is a commandline tool for interfacing with and comprehending knowledge databases as abstracted in the `remd` library. It's goals are:
* The inclusion of a language server with advanced knowledge queries
* The inclusion of a general API used to back the language server as a backend for more sophisticated interfaces - though that will probably end up being placed in the remd library instead.
