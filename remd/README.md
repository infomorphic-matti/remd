# remd
The purpose of this library is the construction of a universe of typed objects and relations to build knowledge databases/graphs, for e.g. documenting the structure of an organisation and it's interactions with other organisations. It's inspired by the concept developed with [tiddlywiki][tiddlywiki], except divorced from the wiki specifics and with more rigorous types and relationships.

[tiddlywiki]: https://tiddlywiki.com/

It essentially acts as a *constructive proof language with dependent types*. That's a lot of buzzwords, but the essential idea is that your knowledge database is a combination of the following:
* A collection of types/values defined to exist. 
* A collection of proofs that construct another type from values/types if the parameter type exists.

It's a bit more complex than that. For instance, this system also allows linking out to external knowledge structures or raw external links and referencing them internally by providing proofs of equivalence - that is, a means of turning the external type/value into one of your own. 

## License
This entire project is licensed under the *AGPLv3 or later*.

## Modules
Naming and structure for an arbitrary knowledge-relationship-mapping construction is difficult. However, we already have a good answer to that problem provided by the very language this library is written in - Rust!

Knowledge databases have - just like Rust - the concept of paths. In particular, a given knowledge network is layed out as the following:
```text
base-folder/
- main.remd
- sub1.remd
- sub2.remd
- sub2/
  - submodule.remd
  - even_more_sub.remd
  - even_more_sub/
    - yay.remd
```

Submodules can either be declared inside an existing module file, or have a subfolder rust-style. However, unlike rust, there is no option for a `mod.rs` style module definition inside a folder.
